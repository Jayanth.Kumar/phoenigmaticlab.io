| **Create a Report** |
| --- |

The Resident makes a report of the status of the patient's illness by comparing the current status of the illness with existing historical data\(images\)

| **Preconditions:** The Resident would be able to pull a patient folder from different data sources as shown in the wireframe below. Verify that the Trainee is authorized to acess the radiology reporting tool by entering username and Password |
| --- |

![](/assets/Wireframe.png)

**Flow of Event**

* The user clicks on the patient's folder to expand and show all the instances of scans the patient has undergone.

* The user clicks a dropdown to group study instances based on modalities and procedure under the same Patient ID\(PID\)

* The user selects the study and any of the other studies to be compared with image under study:

  * **Open:** After selecting all the relevant folder containing series of images in one study, the user clicks 'Open' to compare and write report about the study on the adjacent screen that has a wider view.
  * **Update: **After Completing the  report on the current study, the Resident clicks 'Update' to save the new addition to the record in the report section as shown below. Also, Verify that it gets pushed to the Resident Staff for approval.


**Notes:**

* Study Info: The table shows the general information of the patients on clicking 'Study Info". The Study info works for the main study only and not for any other under ' Other Studies'
* Study Notes: On clicking, the table shows rows of reports that have made with timeline, which is clickable for view
* Reports:On clicking, a new editable template shows up for the Resident to write report.
* Docs: Shows a view of any document relating to the patient: Insurance,etc 
* Hospital: Shows icons of other applications that may contain the patient's record.

![](/assets/Info.png)

![](/assets/Report.png)

![](/assets/Report2.png)

![](/assets/Apps.png)

| **Study Comparison With Historical Images** |
| --- |
| **Preconditions: **Main study and every other study under 'Other Studies' to be compared would have been selected. When the user clicks the 'Open' botton on the middle-right section, the images open in the adjacent screen. |

**Flow of Event**

A Resident  want to be able to view a page containing a list of the patient's historical  images to be compared with the recently ordered report. A Resident want be able to conduct a real-time trasversal comparison between different batches of studies and  write a report on the current study.

* Verify That the list of the images in the studies selected are perfectly ordered based on the metadata that come with the original images. 
* Verify that the 'Open' key disappears as soon as the user clicks it. This should signify that the report is in an editable mode.
* Verify that the images skips to the next image once the user clicks on it on the adjacent screen.
* Verify that the basic editing toolbar floats around the screen for easy access
* Verify that the Resident can make a square shape on an area to zoom-in on the spot of interest

Note:

![](/assets/image1.png)
![](/assets/image2.png)

| **Browsing Through Worklist Through the Hospital Tab** |
| --- |
| **Precondition\(s\):**The application would have been integrated with  the major worklist application- **Synapse,** as shown in tab 'Hospital A'. Clicking Synapse takes the user to the external application which contain all the worklists of all affiliated hospitals. |

