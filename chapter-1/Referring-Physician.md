# 1. Introduction

The following gives a general overview of how a Nurse Practitioner interacts with system on a daily basis. The focus is going to be on how the User places order to the radiology department, an action which is usually taken based on discretion rather than instruction by the Resident Physician.

## 2. Purpose

The purpose of the User Stories is to understand how the user prefers to interact with the order placement methodology basing our report on what currently exist and other things that are currently not existing but could make the order placement easier.

## 3. Intended Audience

The Intended audience is a Nurse practitioner and also a Radiologist PA, who would have gone through the training as a Technologist. These individuals place order to the Technologist to conduct a certain procedure on a particular Patient.

## 4. Use Case

* The user starts by logging in into the system using authorized username and password.
* The user gets directed to a result review page where individual patients can be searched.

* After locating the patient, the user clicks on it to be directed to a page containing vertically arranged tabs containing the following:


**4.1 Laboratory**

Under the Laboratory tab, the user sees a Vital Signs spreadsheet containing horizontally arranged tabs of different procedure sections of the Laboratory etc. Complete Blood Count\(CBC\).This is where the user derives the assertiveness to place any order based on the need of the patient.

![](/assets/Nurse Prac3.png)

**4.2 Order**

The user clicks the Order tab be directed to a page which contains a spreadsheet containing the another set of tabs that are arranged horizontally as in the Laboratory tab above. However, in this page, the spreadsheet shows the current orders that has been made under each of the procedures such as Radiology, Ultrasound etc..

To place order to the Radiology department the user follows the following steps:

The user clicks the Radiology tab and click Add button, which is slightly under the Order Tab \(See the Image below\). A new form is shown for data input. 

** Procedure title** : The user clicks the Favourite tab to select the procedure title \(e.g. CT, US\) from the drop down. If it doesn’t exist, the user types it in the box.

**Additional Procedure Information**:The User Writes a note to made the order clearer to the Radiologist e.g. P.O. Contrast
**Reason for Examination**: The user is required to fill this box to point the Radiologist in the right direction and also to account for the accurate billing of the patient.

**Priority**: The user selects the priority status for the report. E.g. Routine, which means not so urgent

![](/assets/Nurse Prac1.png)

**4.3 Results Review**

The user can always go to the result review tab to check if the report of the procedure has been developed and sent from the Radiology department.

The user clicks the Radiology tab to check if any of the examination ordered has been submitted from the Radiology department

The user clicks on the result to deduce from it.

![](/assets/Nurse Prac2.png)

