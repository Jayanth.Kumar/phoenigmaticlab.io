## **Creation of a Patient Appointment:**

When an patient is prescribed a radiology intervention, the user \(Technologist\) should be able to book an appointment.

### **Details:**

**Parent : **Radiology department and central scheduling department.

**Primary actors : **Radiology Technician and the Central scheduling department users

**Preconditions: **The intervention has been prescribed the Physician and also the radiology technician and the central scheduling department workers have the access to the scheduling application.

**Level :**User

**User cases status**:Primary

**Success guarantee: **The patient's appointment gets schedules successfully with specific time and date.

### **Flow of Events:**

The user enters the scheduling application using the appropriate login credentials assigned to them.

The user locates the patient in the appointment tab using their name or Medical record number.

Once the patient is located, the user adds the imaging modaility to the patient as prescribed.

The user then adds the specific information about the imaging technique and also the other important information like patients allergey and medication information, patient contact number, emergency phone number and person of contact,insurance id number and the CPT code.

Once all the required information is added, the user confirms the appointment and **accession code **is created and also the appointment can be viewed on the time line of the specific date, specific room, specific imaging modality of the department.

![](/assets/Capture_4.PNG)

# Update the Existing Appointment:

When there is need for update of the appointment, the user should be able to able to change the date, time or the room for the intervention.

### **Details:**

**Parent : **Radiology department and central scheduling department.

**Primary actors  **Radiology Technician and the Central scheduling department users

**Preconditions: **The radiology technician and the central scheduling department workers have the access to the scheduling application.

**Level :**User

**User cases status**: Primary

**Success guarantee: **The patient's appointment gets updated with the new scheduling time, date and credntials.

**Flow of Events:**

The user can change the schedule of the appointment when login credentials are prompted.

The user goes into the time line of the specific scheduling book and looks in for the appointment.

Once the appointment is found, the user selects it to highlight and right clicks over the appointment and selects the **update** option to update the appointment and prompts the updated date,time and room.

Once the appointment is updated, the user confirmes it .

If already another appointment is fixed for that specific date, time and intervention room, the system prompts an error message **"Cannot update the appointment".**

The user then updates with appropriate time and date and intervention room.





Updating the appointment status

